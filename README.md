Ember Katas
===========

A suite of katas to experiment with Ember.js

Available Katas
---------------

- [FizzBuzz](katas/fizzbuzz.md)

Constraints ideas
-----------------

- [Component Mania](constraints/component-mania.md)
- Use TDD (Ember QUnit or Ember Mocha)
- Use ATDD (Guerkin files and Ember Yadda)
- Use input validation
- Use typescript
- Use mirage.js
- Use animations
- Use components
FizzBuzz
========

Write an ember.js app that : 
- ask user for an integer
- if the input is a multiple of three, display 'Fizz'
- if the input is a multiple of five, display 'Buzz'
- otherwise display the integer


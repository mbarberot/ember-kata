Component Mania
===============

Use components to break the template.

Rules
-----

- HTML elements must be wrapped
- Do not nest more then one level

Examples
--------
FAIL : 
  - HTML elements must be wrapped
  - Nesting level > 1
```handlebars
<div class="item">
    <h1>The Lord Of The Ring</h1>
    <button>Add to cart</button>
    <div class="list reviews">
        {{#each this.reviews as |review|}}
            <div class="review">
                <div class="note">{{review.note}}/10</div>
                <div class="author">{{review.user}}</div>
            </div>
        {{/each}}
    </div>
</div>
```

FAIL : 
  - some HTML elements are still not wrapped
```handlebars
<div class="item">
    <h1>The Lord Of The Rings</h1>
    <button>Add to cart</button>
    <Reviews @reviews={{this.reviews}} />
</div>
```

PASS 
```handlebars
<Item @title="The Lord Of The Rings">
    <AddToCart />
    <Reviews @reviews={{this.reviews}} />
</Item>
```

But do not forget to apply these rules to the new components !

FAIL : 
  - HTML elements must be wrapped
  - Nesting level > 1
```handlebars
<!-- components/reviews  -->
<div class="list reviews">
    {{#each this.reviews as |review|}}
    <div class="review">
        <div class="note">{{review.note}}/10</div>
        <div class="author">{{review.user}}</div>
    </div>
    {{/each}}
</div>

<!-- components/add-to-cart -->
<button>Add to cart</button>

<!-- components/item -->
<div class="item">
    <h1>{{@title}}</h1>
    {{yield}}
</div>
```

PASS
```handlebars
<!-- components/reviews  -->
{{#each this.reviews as |review|}}
    <Review @review={{review}} />
{{/each}}

<!-- components/review -->
<Note @note={{@review.note}} />
<Author @author={{@review.user}} />

<!-- components/note -->
<div class="note">{{@note}}/10</div>

<!-- componeents/author -->
<div class="author">{{@author}}</div>

<!-- components/add-to-cart -->
<button>Add to cart</button>

<!-- components/item -->
<div class="item">
    <Title @text={{@title}}
    {{yield}}
</div>

<!-- components/title -->
<h1>{{@text}}</h1>
```
